create or replace FUNCTION fn_get_mv_last_refresh (p_mv_name IN VARCHAR)
RETURN 
VARCHAR
AS last_refresh VARCHAR(40);
v_mv_name VARCHAR(40) := p_mv_name;
BEGIN
SELECT 
    to_char(last_refresh_date, 'YYYY-MM-DD HH24:MI:SS') last_refresh_date
INTO last_refresh
FROM 
    all_mviews
WHERE (1=1)
AND mview_name = v_mv_name;
RETURN last_refresh;
END;